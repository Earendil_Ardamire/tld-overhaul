# TLD Overhaul

Module System (Source Code) of the LotR-Modding Project TLD Overhaul

Licensed under CC-BY-NC-SA 3.0.

This means that every improvement based on our work must be shared with others
under the same license, credit must be provided without appearing as
endorsement, and that nobody can profit from it. You are free to share, copy,
remix and adapt it as you wish.

Mothermod TLD: https://github.com/tldmod/tldmod/tree/master/ModuleSystem